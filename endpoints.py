# Standard Libs
import sys
import os
from os import getenv
import pwd
import json
import time
import socket
from datetime import datetime, timedelta
import pytz
from typing import Optional, List, Dict
import asyncio
from asyncio import gather, all_tasks, current_task
from tempfile import gettempdir
import fcntl
import gzip

# External Libs
import uvicorn
import aiofiles
import httptools
import httpx
import uvloop
from pydantic import BaseModel
from fastapi import FastAPI, Depends, HTTPException, Request, Query
from fastapi.security import HTTPBasic, HTTPBasicCredentials
from fastapi.encoders import jsonable_encoder
from fastapi.responses import JSONResponse, ORJSONResponse
from starlette.requests import Headers
from starlette.middleware.base import BaseHTTPMiddleware, RequestResponseEndpoint
from starlette.requests import Request
from starlette.responses import Response

from loguru import logger
from dotenv import load_dotenv

# Local application imports

HOST = "0.0.0.0"
PORT = 8095
ROOT_PATH = "/kbfinancerepsapiservice"

# Set up basic authentication
security = HTTPBasic()
uvloop.install()
load_dotenv()

# Ensure we are relating imports to are current directory
current_dir = os.path.dirname(os.path.abspath(__file__))
parent_dir = os.path.dirname(current_dir)
sys.path.append(current_dir)

# Set up logging
log_file = os.path.join(current_dir, 'log', 'kb-finance-reps-api-service.log')
logs = logger.add(
    log_file,
    format="{time:YYYY-MM-DD at HH:mm:ss} {exception} | {level} | {message} | {line} | {module}",
    level="INFO",
    enqueue=True
)

class RootPathMiddleware(BaseHTTPMiddleware):
    async def dispatch(
        self, request: Request, call_next: RequestResponseEndpoint
    ) -> Response:
        if "kbfinancerepsapiservice" in request.url.path:
            request.scope["path"] = request.url.path.replace("/kbfinancerepsapiservice", "")
        return await call_next(request)
    

app = FastAPI(
    openapi_url="/openapi.json",
    docs_url="/docs",
    redoc_url="/redoc",
    default_response_class=ORJSONResponse
)

app.add_middleware(RootPathMiddleware)

def verify_credentials(credentials: HTTPBasicCredentials = Depends(security)):
    correct_username = getenv('BASIC_AUTH_USERNAME')
    correct_password = getenv('BASIC_AUTH_PASSWORD')
    if credentials.username != correct_username or credentials.password != correct_password:
        raise HTTPException(
            status_code=401,
            detail="Incorrect username or password"
        )
    return credentials


def process_headers(headers: Headers) -> dict:
    required_headers = ["referer", "x-kuali-origin", "x-kuali-user-id", "x-kuali-user-ssoid", "X-Response-URL", "X-Status-Code"]
    return {header: headers.get(header) for header in required_headers}


@app.middleware("http")
async def add_process_time_header(request: Request, call_next):
    headers = process_headers(request.headers)
    referer = headers.get('referer') or f"{headers['x-kuali-origin']} | {headers['x-kuali-user-id']} | {headers['x-kuali-user-ssoid']}"
    query_string = request.query_params._list

    start_time = time.time()
    try:
        response = await call_next(request)
        process_time_str = str(timedelta(seconds=time.time() - start_time))
        response.headers["X-Process-Time"] = process_time_str

        log_msg = f"{referer} | {query_string} | {process_time_str} | {request.url.path} | {response.status_code}"
        logger.success(log_msg)
        return response
    except Exception as dammit:
        error_msg = f"{referer} | {query_string} | |{request.url.path} | | {str(dammit)}"
        logger.error(error_msg)
        raise dammit


async def cleanup_logs(days=14):
    now = datetime.now(pytz.timezone('US/Eastern'))
    formatted_time = now.strftime("%Y-%m-%d")
    cutoff = now - timedelta(days=days)
    cutoff = cutoff.replace(tzinfo=None)

    async with aiofiles.open(log_file, mode='r') as f:
        lines = await f.readlines()

    new_lines = []
    for line in lines:
        elements = line.split(" at ", 1)
        log_time_str = " ".join(elements[:1])
        try:
            log_time = datetime.strptime(log_time_str, "%Y-%m-%d")
        except ValueError:
            continue

        if log_time > cutoff:
            new_lines.append(line)

    async with aiofiles.open(log_file, mode='w') as f:
        await f.write(''.join(new_lines))

LOCKFILE = os.path.join(current_dir, 'Workers.lock')

async def follow_redirect(url: str):
    async with httpx.AsyncClient(http2=True, follow_redirects=False) as client:
        response = await client.get(url, timeout=30.0)
        if response.is_redirect:
            url = response.headers["Location"]
            header = {'X-Auth-Token': getenv('DATA_EXTRACT_API_KEY')}
            response = await client.get(url, timeout=30.0, headers=header)
        items = response.json()
        items_list = [item for item in items]

        current_dir = os.path.dirname(os.path.abspath(__file__))
        async with aiofiles.open(os.path.join(current_dir, 'reps.json'), mode='w') as f:
            await f.write(json.dumps(items_list))
        return {"message": "Org Unit Structure.xlsx saved successfully!"}


@app.on_event("startup")
async def startup_event():
    with open(LOCKFILE, 'w') as f:
        try:
            fcntl.flock(f, fcntl.LOCK_EX | fcntl.LOCK_NB)
        except IOError:
            return
        await cleanup_logs()
        logger.info("Logs scrubbed")
        logger.info("Fetching OIT Cost Centers...")
        get_live_excel_data = await follow_redirect('https://duke.is/m/tuyb')
        logger.info(get_live_excel_data)
        uid = os.getuid()
        username = pwd.getpwuid(uid).pw_name
        hostname = socket.gethostname()
        logger.info(f"Initializing on host {hostname}")
        logger.info(f"Parent directory is {parent_dir}")
        logger.info(f"Current directory is {current_dir}")
        logger.info(f"User is {username}")
        logger.success(f"Application Started on {HOST}:{PORT}")
        logger.info(f"Route: {ROOT_PATH}/*")


@app.on_event("shutdown")
async def shutdown_event():
    tasks = [t for t in asyncio.all_tasks() if t is not asyncio.current_task()]
    [task.cancel() for task in tasks]
    try:
        await asyncio.gather(*tasks, return_exceptions=False)
    except asyncio.exceptions.CancelledError:
        logger.opt(exception=True).debug("A task was cancelled")
    try:
        os.remove(LOCKFILE)
    except asyncio.exceptions.CancelledError:
        logger.opt(exception=True).debug("LOCKFILE was not removed...")

    logger.success(f"Cancelled {len(tasks)} outstanding tasks")


class Item(BaseModel):
    result: str


@app.post("/api/v1/post", dependencies=[Depends(verify_credentials)], tags=["OIT Cost Centers - Updates"])
async def create_item(items: List[Item]):
    items_list = [item.dict() for item in items]
    async with aiofiles.open(os.path.join(current_dir, 'reps.json'), mode='w') as f:
        await f.write(json.dumps(items_list))
    return {"message": "Items saved successfully!"}


@app.get("/api/v1/get", dependencies=[Depends(verify_credentials)], tags=["OIT Cost Centers - Output"])
async def read_items(filter: Optional[str] = None):
    async with aiofiles.open(os.path.join(current_dir, 'reps.json'), mode='r') as f:
        items = json.loads(await f.read())
    if filter:
        items = [item for item in items if filter.lower() in item.get("cost_center", "").lower()]
    return items


if __name__ == "__main__":
    uvicorn.run(
        __name__ + ":app",
        host=HOST,
        port=PORT,
        proxy_headers=True,
        reload=False,
        root_path=ROOT_PATH,
        loop="uvloop",
        http="httptools",
        workers=2,
        log_level="info",
        timeout_keep_alive=int(os.environ.get("KEEP_ALIVE", 60)),
        limit_max_requests=int(os.environ.get("MAX_REQUESTS", 1000)),
    )