# FastAPI: KB Finance Reps / OIT Cost Centers API Service

This project is a web service that provides an HTTP REST API using the FastAPI framework. Using an Excel Sheet hosted in either Box or Microsoft, we take the data weekly (Sunday @ 3AM) and send a post request from Microsoft Azure to update this data within a file `reps.josn` on the host container.  When a user then requests this data from this service - the response is nearly instant and ensures static/simple results.

This project contains instructions for 2 containers - 

1. web-kb-finance-reps-api-service
2. caddy-kb-finance-reps-api-service

## Project Structure

The main project file is `endpoints.py` which contains the application setup, middleware, routes and business logic. It uses the FastAPI framework to create the HTTP API, and Pydantic for data validation.

## Installation

Before you can run this project, you'll need to install the dependencies. You can do this using pip:

## Docker Compose

** Please note:  $CI_PROJECT_NAME is the project name as named for this project directory in GitLab

1. The `web` service:
    - The Docker image is built from the current directory, tagged as `kuali-$CI_PROJECT_NAME`.
    - The container is named `web-$CI_PROJECT_NAME`.
    - The container exposes port 8095 to the host machine at port 0.0.0.0:8095.
    - Two volumes are mounted to the container. The first volume mounts the `.env` configuration file from the host machine to the `/app/.env` path in the container. The second volume mounts the log directory from the host machine to the `/app/log/` path in the container.
    - The container will be restarted automatically unless it is explicitly stopped.
    - The container is connected to the `caddy_network` network.

2. The `caddy` service:
    - The Docker image used is the latest Caddy server image.
    - The container is named `caddy-$CI_PROJECT_NAME`.
    - The container is set to always restart.
    - Three volumes are mounted to the container. The first volume mounts the `Caddyfile` from the host machine to `/etc/caddy/Caddyfile` in the container. The second volume mounts the `/etc/caddy` directory from the host to `/etc/caddy` in the container. The third volume mounts the Docker socket, allowing Caddy to interact with Docker.
    - The container exposes ports 80, 443, and 443/udp to the host machine at ports 7095, 8449, and 8449 respectively.
    - The container is connected to the `caddy_network` network.

3. The `caddy_network` network:
    - A Docker network named `caddy_network` is created with the bridge driver.

The configuration allows our two services to communicate with each other over the caddy network. In short, our web service runs the FastAPI application, while the caddy service runs a Caddy server that acts as a reverse proxy for this project.  The other benefit of Caddy is the use of automatically obtained/renewed SSL/TLS certificates from Let's Encrypt, and also handles OCSP stapling.  Caddy also uses modern protocols and technologies like HTTP/2 and QUIC to improve both speeds + reliability of our applications (speed increase of nearly 6x HTTP/1.1 in my testing).

You can learn more about caddy and how awesome it is at https://caddyserver.com/docs/

- Please note that you will need to create your own `/etc/caddy/Caddyfile`, `/etc/caddy`, and `/srv/persistent-data/app/$CI_PROJECT_NAME/config/env/.env` files and directories on your host machine as they are required by the Docker configuration.

## Environment variables

```ruby
- BASIC_AUTH_USERNAME='xxx_create_username_xxx'
- BASIC_AUTH_PASSWORD='xxx_create_password_xxx'
```

Using TypeScript, Office Scripts, & Power Automate to schedule automatic extractions from Excel file:

```ts
function main(workbook: ExcelScript.Workbook): { result: string }[] {
  const sheet = workbook.getWorksheet("Revised September 2022");

  const usedRange = sheet.getUsedRange();
  const thisColumn = usedRange ? usedRange.getColumn(6) : null;

  if (!thisColumn) {
    console.log('Column G is empty 😱');
    return [];
  }

  const values = thisColumn.getValues();

  let flatArray: string[] = [];
  for (let i = 0; i < values.length; i++) {
    for (let j = 0; j < values[i].length; j++) {
      flatArray.push(String(values[i][j]));
    }
  }

  let uniqueArray: string[] = [];
  for (let i = 0; i < flatArray.length; i++) {
    let trimmedString = flatArray[i].trim();
    if (trimmedString !== "" && uniqueArray.indexOf(trimmedString) === -1) {
      uniqueArray.push(trimmedString);
    }
  }

  uniqueArray.sort();

  if (uniqueArray.length === 0) {
    uniqueArray = ["Cost Center Not Listed"];
  }

  let returnObjects: { result: string }[] = [];
  for (let i = 0; i < uniqueArray.length; i++) {
    returnObjects.push({ result: uniqueArray[i] });
  }
  // as you see below - we will append final sorted results with "Cost Center Not Listed"
  returnObjects.push({ result: "Cost Center Not Listed" });

  console.log(JSON.stringify(returnObjects));
  return returnObjects;
}
```

## Installation

1. Clone the repository to your local machine
2. Navigate to the project directory in a terminal.
3. Build the Docker image using the following command:

```bash
docker-compose build
```

## Usage

- To test the application in a Docker container, use the following command:

```bash
docker-compose up
```

- To run the application in a Docker container, use the following command:

```bash
docker-compose up -d
```